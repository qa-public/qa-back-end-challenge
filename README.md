## Mock Key-Value store

Before your interview, write a program that runs a server that is accessible on http://localhost:4000/. When your server receives a request on http://localhost:4000/set?somekey=somevalue it should store the passed key and value in memory. When it receives a request on http://localhost:4000/get?key=somekey it should return the value stored at somekey. Preferable would be the persistence of data(store it to a file, etc. You can start with simply appending each write to the file, and work on making it more efficient if you have time).

*Bonus points*: Write good comments, clean code, and UnitTests if time permits.

### How to deliver the solution

We expect you to share a repository with us with your solution pushed.
Keep the commit history clean. ;)

## What comes after

Once you have done your challenge and submitted it, we will take some time to review and will ask you to join us for an in-person technical interview (at our office or On-line) if found satisfactory.  During your interview, you will pair on improving your KV store with one of our developers.
